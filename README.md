# Kozmik Günce

This repository contains source files for a Turkish language science blog [KozmikGünce](https://www.kozmikgunce.com). It is based on the open source [Jekyll](https://jekyllrb.com/) static site generator and uses [Beautiful Jekyll](http://deanattali.com/beautiful-jekyll/) theme by [Dean Attali](http://deanattali.com/).
