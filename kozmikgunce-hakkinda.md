---
layout: page
title: Kozmik Günce Hakkında
#subtitle: Why you'd want to go on a date with me
---
![Singing Boy]({{ site.url }}/assets/vault-boy-swap.jpg)

Boğaziçi Üniversitesi ve İTÜ dersliklerinde tanışmış genç fizikçileriz. Aklımıza takılanları ya da bir zamanlar takılıp sonra çözülenleri fırsat buldukça paylaşmak istiyoruz. Öncelikli alanlar astrofizik, matematiksel fizik ve kozmoloji.

Yazılar, içerikleri çok çeşitli olmakla beraber, bir ortak noktaya sahip olacak: Bilim. Hakemli dergiler gibi değil elbette. Popüler bilim haberleri yapan sitelere de benzemeyecektir. Burada, elimizden geldiğince, ilgilendiğimiz ve başkalarının da ilgilenebileceği veya başımıza gelen ve başkalarının da başına gelebilecek olaylara, konulara değineceğiz.

İyi seyirler.

---

**Cem Eröncel**  
[Syracuse Üniversitesi](http://physics.syr.edu/people/grad-students.html), Fizik doktora adayı  
cemeroncel at gmail dot com  
  
**Devin Çeşmecioğlu**  
[Massachusetts Üniversitesi](https://www.physics.umass.edu/directory/graduate-students), Fizik doktora adayı  
devinces at gmail dot com  