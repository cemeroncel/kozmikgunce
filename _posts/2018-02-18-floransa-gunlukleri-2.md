---
layout: post
title: Floransa Günlükleri - Hafta 1
subtitle: GGI 2018 Kış Okulu'ndan İzlenimler
tags: [standart-model parçacık-fiziği]
author: Cem Eröncel
comments: true
published: true
---
Bir önceki [yazımda]({{ site.baseurl }}{% link _posts/2018-01-08-floransa-gunlukleri-1.md %}) belirttiğim gibi 8 - 27 Ocak tarihleri arasında İtalya'nın Floransa kentinde gerçekleştirilen *GGI Lectures on the Theory of Fundamental Interactions* okulundaydım. Hedefim bu günlükleri okul sırasında yazmaktı, ama okul süresince kendi araştırma projelerime de zaman ayırmam gerektiği için, ancak bu zamana kaldı bu yazıları yazmak. 

Bu yazıyla birlikte toplam üç yazı olacak olan bu seride, okulda ders olarak gördüğüm 6 farklı konunun kendimce en önemli ve temel kısımlarını aktarmaya çalışacağım. Tabi ki her biri birer kitap olabilecek konuları birkaç paragrafta bütün yönleriyle ele almak mümkün değil, ama genel bir fikir sahibi olunması açısından yararlı olacaklarını umuyorum. Keyifli okumalar. 

## Elektrozayıf Etkileşimler

*Zayıf etkileşim* kavramı, ilk olarak \\(\beta\\)-*bozunumu* ve *müon bozunumu* olaylarının gözlemlenmesiyle fizik dünyasındaki yerini aldı. \\(\beta\\)-bozunumu, kararsız bir atomun içersindeki nötronlardan birinin, birer adet elektron ve anti-elektron-nötrinosu yayarak protona dönüşmesidir. Müon bozunumunda ise, müon parçacığı birer adet müon-nötrinosu ile anti-elektron-nötrinosu yayarak elektrona dönüşür. O zamana kadar parçacıklar arasındaki etkileşimlerden sadece elektromanyetizma biliniyordu ve bu fiziksel olayları açıklamak elektromanyetizma ile mümkün değildi. 

Zayif etkileşimleri anlamak için ilk girişim, 1934 yılında Enrico Fermi'den geldi. [^1] Fermi teorisi, ikisi parçacık, ikisi anti-parçacık olmak üzere toplam dört adet fermiyonun etkileşimi üzerine kurulu idi. Etkileşimin kuvveti, *Fermi sabiti* denilen bir katsayı ile orantılıydı ve bu katsayıyı müon bozunumu deneylerinden hassas bir şekilde hesaplamak mümkündü:

$$ G_F = 1.1663787 \times 10^{-5}\;\textrm{GeV}^{-2}$$

Bu etkileşime zayıf etkileşim denmesinin sebebi ise, katsayının küçüklüğünden de anlaşılabilineceği gibi, elektromanyetizmaya göre çok daha zayıf olması idi. Fermi kuramı, \\(\beta\\)-bozunumu ile müon bozunumunu çok iyi açıklasa da, zayıf etkileşimlerin gerçek kuramı olamazdı, çünkü yüksek enerjilere çıkıldığında geçerliliğini yitiriyordu. Alan kuramına biraz aşina okuyucular için bunun sebebinin Fermi sabitinin sıfırdan küçük kütle boyutuna sahip olması olduğunu belirtelim. 

Gerçek kuram, 1967 yılında Sheldon Glashow ve Abdus Salam'ın da yardımlarıyla Steven Weinberg tarafından geliştirildi. [^2] Bu kuramın en önemli özelliği, zayıf etkileşimler ile elektromanyetizmayı tek bir çatı altında toplamasıydı. Bu iki kuvvet, düşük enerjilerde farklı kuvvetler olmasına rağmen, yüksek enerjilerde *elektrozayıf kuvvet* denilen tek bir kuvvette birleşiyordu. Bu birleşim, kuramsal fizikçilerin hayallerini süsleyen, dört kuvveti tek bir çatı altında toplayacağı düşünülen *herşeyin kuramı*'nın ilk adımı sayılabilir. 

Dersin ilk bölümünde, bu kuramın \\(SU(2)\otimes U(1)\\) ayar kuramı üzerinden nasıl kurgulanacağı ve genel yapısı tartışıldı. İkinci bölümde ise, yüksek enerjilerdeki *elektrozayıf kuvvet*'in, düşük enerjilerde nasıl elektromanyetik ve zayıf kuvvet olarak ayrıldığını açıklayan *ayar simetrsinin doğal olarak kırınımı* kuramı ayrıntılı olarak işlendi. Ders, ilk [yazımın]({{ site.baseurl }}{% link _posts/2017-12-10-neler-oluyor.md %}) da konusu olan, Standart Model'in sorunlarına kısa bir bakış ile sona erdi. 

Konuyla ilgilenenler için birkaç kaynak:

- H. Georgi, Weak Interactions and Modern Particle Theory (Dover Publications, Mineola, N.Y, 2009).
- C.M. Becchi and G. Ridolfi, An Introduction to Relativistic Processes and the Standard Model of Electroweak Interactions (Springer, Boston, MA, 2014).

## Efektif Alan Kuramı ##

Fizikte bir sistemin tüm enerji seviyelerinde geçerli olan analitik bir çözümünün olduğu durumlar oldukça enderdir. Kimi zaman bütün kuramı bilmemize rağmen, denklemlerin analitik çözümlerini birkaç ideal durum dışında bulamayız, kimi zaman da elimizdeki kuram ancak belirli enerji seviyelerine kadar geçerliliğini korur. İlk duruma örnek olarak Genel Görelilik'i, ikinci duruma örnek olarak da Standart Model'i verebiliriz. 

*Efektif Alan Kuramı*'nı, *bilmediğimiz* fiziği *bildiğimiz* fiziğin içine entegre etmenin sistematik bir yöntemi olarak ifade edebiliriz. Bunun için öncelikle sistemin davranışını ifade etmek istediğimiz bir enerji seviyesi seçilir ve sistem, seçtiğimiz enerji seviyesinde geçerli olacak şekilde çeşitli parametreler cinsinden ifade edilir. Bu sırada, seçtiğimiz enerji seviyesinden daha yüksek enerjilere sahip serbestlik derecelerinin sistem üzerine etkileri, yazdığımız *efektif* kuramın parametrelerinin içine entegre edilmiş olur. Yani elimizdeki kuramın yüksek enerji seviyesindeki davranışı, düşük enerji efektif kuramdaki parametreler cinsinden ifade edilir. 

Örnek olarak Standart Model'i düşünelim. İlk [yazımda]({{ site.baseurl }}{% link _posts/2017-12-10-neler-oluyor.md %}) belirttiğim nedenlerden ötürü Standart Model'in tüm enerji seviyelerinde geçerli olabilecek bir kuram olmadığını biliyoruz. Yani Standart Model de bir *efektif alan kuramı*'dır. Standart Model'in geçerliliğini yitirdiği enerji seviyelerindeki fiziğin etkileri, Standart Model'in parametreleri içerisinde gizlenmiştir. Bu parametrelerin nasıl hesaplanacağını bilemesek de, deneyler vasıtasıyla ne olmaları gerektiğini bulabiliriz. Bu da bize Standart Model'i "düşük" enerjilerde geçerli efektif bir kuram olarak ele alıp, bu enerjilerde hesap yapabilmemizi sağlar. 

Efektif Alan Kuramı, yalnızca genel kuramı bilemediğimiz durumlar için yararlı değildir. Genel kuramı biliyor olmamıza rağmen, analitik çözüme sahip olamıyor olabiliriz. Bu durumda da, Efektif Alan Kuramı bize problemi basitleştirerek ele almamızı sağlar. Bunun en güncel örneği, son zamanlarda oldukça popüler bir araştırma konusu olan, nötron yıldızı birleşimlerinin Efektif Alan Kuramı yöntemleri kullanılarak incelenmesidir. 

Bilindiği üzere, ilk nötron yıldızı birleşmesi geçtiğimiz yıl LIGO/Virgo deneyi tarafından gözlendi.[^3] Bu gözlemden elde edilen kütleçekim dalgası, nötron yıldızının iç yapısı hakkında önemli ipuçları barındırıyor. Dolayısıyla, nötron yıldızlarının iç yapısının, birleşmeden doğan kütleçekim dalgasına ne gibi etkilerinin olabileceğini dikkatlice hesaplamak, önümüzdeki yılların en önemli araştırma konularından biri olmaya aday. Efektif Alan Kuramı da, bir yöntem olarak bu araştırmalarda kilit rol oynayacağa benziyor. 

Derste Goldstone Teoremi'nden başlanarak, Efektif Alan Kuramı'nın nasıl kurulacağı ayrıntılı olarak ele alındı. Ardından, son derece oturmuş bir kuram olan Klasik Elektrodinamik, Efektif Alan Kuramı çerçevesinden incelendi. Son olarak, yukarıda paragrafda bahsettiğim gibi, bu yöntemin nötron yıldızı birleşimlerine nasıl uygulanabileği konusuna kısa bir girişle ders sona erdi. 

Konuya ilgi duyanlar için, ders boyunca verilen birkaç kaynağı da ekleyelim:

- I.Z. Rothstein, [arXiv:hep-ph/0308266](https://arxiv.org/abs/hep-ph/0308266) (2003).
- R.P. Woodard, [arXiv:hep-th/1506.02210](https://arxiv.org/abs/1506.02210) (2015).
- L.V. Delacrétaz, S. Endlich, A. Monin, R. Penco, and F. Riva, J. High Energ. Phys. 2014, 8 (2014). [arXiv:hep-th/1405.7384](https://arxiv.org/abs/1405.7384)
- I.Z. Rothstein and P. Shrivastava, [arXiv:hep-th/1712.07795](https://arxiv.org/abs/1712.07795) (2017).
- G. Goon, A. Joyce, and M. Trodden, Phys. Rev. D 90, 025022 (2014). [arXiv:hep-th/1405.5532](https://arxiv.org/abs/1405.5532)

## Gelecek Yazı ##

Gelecek hafta okulun ikinci haftasındaki dersleri özetleyeceğim. 

## Referanslar ##

[^1]: [E. Fermi, Z. Physik 88, 161 (1934). ](https://link.springer.com/article/10.1007%2FBF01351864). İngilizce çevirisi için [F.L. Wilson, American Journal of Physics 36, 1150 (1968).](http://aapt.scitation.org/doi/10.1119/1.1974382)

[^2]: [S. Weinberg, Phys. Rev. Lett. 19, 1264 (1967).](https://journals.aps.org/prl/abstract/10.1103/PhysRevLett.19.1264)

[^3]: [LIGO Scientific Collaboration and Virgo Collaboration, Phys. Rev. Lett. 119, 161101 (2017).](https://journals.aps.org/prl/abstract/10.1103/PhysRevLett.119.161101)
