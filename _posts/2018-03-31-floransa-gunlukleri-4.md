---
layout: post
title: Floransa Günlükleri - Hafta 3
subtitle: GGI 2018 Kış Okulu'ndan İzlenimler
tags: [standart-model parçacık-fiziği]
author: Cem Eröncel
comments: true
published: true
share-img: /assets/firenze.jpg
---
GGI 2018 izlenimlerimi aktardığım yazı dizisine, okulun son haftası ile noktayı koyuyoruz. Bu haftanın konuları Antonio Riotto'dan *Enflasyon ve Kozmolojik Pertürbasyonlar* ve Tracy Slatyer'dan *Karanlık madde*.

## Enflasyon ve Kozmolojik Pertürbasyonlar ##

Günümüzde *Kozmoloji*, veya daha Türkçe adıyla *evren bilim*, evrenin büyük ölçeklerde homojen ve izotropik olduğu kabulü üzerine kuruludur. Bu kabul, evren yaklaşık 400,000 yaşındayken elektron ve protonların birleşerek hidrojen atomunu oluşturmaları sonucu oluşan *kozmik arkafon ışınımı* gözlemleri tarafından da desteklenmektedir. 

Bununla birlikte, evrenin zamanla genişlediğini Edwin Hubble'ın 1930 yılında yaptığı gözlemlerden biliyoruz. Homojen ve izotropik evren kabulünü evrenin zamanla genişlemesi ile birleştirirsek, evrenin geometrisini belirleyen *metrik* fonksiyonunu

$$ ds^2=-dt^2+a^2(t)d\vec{x}^2 $$

şeklinde yazabiliriz. Bu metriğe, *FRW metriği* denir ve günümüz evren biliminin temelini oluşturur. Bu ifadedeki zamana bağlı \\( a(t) \\) fonksiyonuna *büyüme faktörü* denir ve evrenin nasıl ve ne hızda genişlemesi gerektiğini bu faktör belirler. Bu fonksiyon da, Einstein denklemleri aracılığı ile evrenin sahip olduğu enerji ve basınç yoğunluğuna bağlıdır. Bu bağıntı da *Friedman denklemleri* aracılığı ile formalize edilir. 

Ancak bu basit model, gözlemlediğimiz evreni açıklamak için yeterli değil. Bu modelin öngörülerini, kozmik arkafon ışınımından çıkardığımız sonuçlarla karşılaştırdığımızda birtakım tutarsızlıklarla karşılaşıyoruz. Bunlara değinmek yazıyı çok uzatacağından, bu sorunların neler olduklarını başka bir yazıya bırakalım. 

*Enflasyon kuramı*, yukarıda özetiğini verdiğim evren modeline ek olarak, evrenin başlangıçta çok büyük bir hızla üstel olarak genişlediği, *enflasyon* dediğimiz bir dönemden geçmiş olduğunu varsayar. Bu hızlı büyüme evresi, basit modelin yol açtığı tutarsızlıkları başarılı bir şekilde açıklayabilmiştir. Bu kuram henüz gözlemsel olarak kanıtlanamasa da, bir çok fizikçi tarafından kabul görür. 

Peki evreni homojen ve izotropik bir sistem olarak kabul ediyorsak, galaksileri, yıldızları ve gezegenleri nasıl açıklayabiliriz? Bunun yolu, evrenin homojen ve izotropik ideal durumdan ufak sapmaları olduğunu kabul etmektir, ki kozmik arkafon ışınımı da bize bunu söyler. Bu küçük sapmalara *kozmolojik pertürbasyonlar* denir. Evrende gözlemlediğimiz yapıların (galaksi, yıldızlar, vb.), mikro ölçekteki bu sapmaların enflasyon ile makro ölçeğe genişlemesi sayesinde oluştuğu görüşü, fizikçiler arasında yaygın bir varsayımdır. 

Antonio Riotto dersine basit evren modeline kısa bir girişle başladı. Ardından bu modelin getirdiği problemlerden bahsedip, enflasyon kuramının bu problemleri nasıl çözdüğünü açıkladı. Son olarak da, kozmolojik pertürbasyon kuramının temellerini ve nasıl uygulanabileceğini detaylı bir şekilde işledi. 

Konuyla ilgilenenler ders notlarına [bu bağlantıdan](http://webtheory.sns.it/ggilectures2018/riotto/riotto.pdf) ulaşabilirler. 

Diğer kaynaklar:

- D. Baumann, [arXiv:hep-th/0907.5452](https://arxiv.org/abs/0907.5424) (2012).
- D.H. Lyth and A. Riotto, Physics Reports 314, 1 [arXiv:hep-ph/9807278](https://arxiv.org/abs/hep-ph/9807278) (1999).
- A. Linde, Prog Theor Phys 163, 295 [arXiv:hep-th/0503195](https://arxiv.org/abs/hep-th/0503195) (2006).



## Karanlık Madde ##

Karanlık maddenin ne olduğuna, *Neler Oluyor?* başlıklı [yazımda]({{ site.baseurl }}{% link _posts/2017-12-10-neler-oluyor.md %}) değinmiştim. Dolayısıyla konu özeti kısmını atlayarak, direk olarak derste hangi konuların işlendiğinden bahsedeceğim. 

Tracy Slatyer dersine karanlık maddenin varlığına dair deneysel motivasyonları sıralayarak başladı. Ardından herhangi bir karanlık madde kuramının sağlaması gereken genel koşullardan bahsederek, karanlık maddeyi oluşturan parçacığın hangi kütlelere sahip olabileceğinden konuştu. Bu zamana kadar ortaya atılmış ve halen deneysel olarak ne doğrulanmış ne de yanlışlanmış sayısız karanlık madde modelini kısaca özetledikten sonra, karanlık maddeyi deneysel olarak nasıl aradığımızdan bahsederek dersini sonlandırdı. 

Kendi derlediğim birkaç kaynak

- M. Lisanti, [arXiv:astro-ph/1603.03797](https://arxiv.org/abs/1603.03797) (2017).
- *US Cosmic Visions: New Ideas in Dark Matter 2017: Community Report* [arXiv:astro-ph/1707.04591](https://arxiv.org/abs/1707.04591) (2017).
- *Dark Sectors 2016 Workshop: Community Report* [arXiv:hep-ph/1608.08632](https://arxiv.org/abs/1608.08632)


## GGI 2018 Dizisini Bitirirken... ##

Bu yazıyla birlikte, GGI 2018 kış okulunda edindiğim izlenimlerimi aktardığım yazı dizisi de sona ermiş oldu. Herbiri sayısız yazı dizisine konu olabilecek birbirinden farklı konuları, bir günce yazısında anlatmaya çalışmak tahmin ettiğimden çok daha zor bir iş oldu benim için. Bunda ne kadar başarılı oldum bilemiyorum. Ama ilgilenen insanlara, Dünya'nın önde gelen fizik kurumlarında ne gibi sorunların çalışıldığı hakkında ufak bir fikir verebildiğimi düşünüyorum. 



