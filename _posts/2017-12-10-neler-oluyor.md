---
layout: post
title: Neler Oluyor?
subtitle: Standart Model'in Sorunlarına Bir Bakış
tags: [standart-model parçacık-fiziği]
author: Cem Eröncel
comments: true
published: true
---

Kuramsal Yüksek Enerji Fiziği'nin, geçmiş yüzyıldaki en büyük başarısı olarak *Standart Model*'i gösterebiliriz. Bu model, atomaltı parçacıkların özelliklerini ve birbirleriyle olan etkileşimlerini neredeyse mükemmel diyebileceğimiz bir hassasiyetle açıklar. 2012 yılının Temmuz ayında *Higgs Parçacığı*'nın CERN'deki *ATLAS* ve *CMS* deneylerindeki keşfi ile birlikte, bu modelin öngördüğü tüm parçacıklar deney tarafından doğrulanmış oldu. Ancak bu keşif, kuramsal fizikçiler için yolun sonu değil, henüz yalnızca başı.

Bu yazımda, Standart Model'in sorunlarından kısaca bahsederek, neden Standart Model'in ötesinde bir kuramın var olması gerektiğini açıklamaya çalışacağım.

## Hiyerarşi Problemi ##

ATLAS ve CMS deneyleri, yaptıkları ölçümlerden yola çıkarak Higgs parçacığının kütlesinin yaklaşık olarak 125 GeV olduğunu belirlediler. Bu değer, diğer parçacıklarla karşılaştırıldığında küçük olmasa da, Standart Model bize Higgs kütlesinin bu kadar küçük olmaması gerektiğini gösteriyor. Bu soruna *Hiyerarşi Problemi* deniliyor.

Aslında Standart Model Higgs parçacığının kütlesi için belli bir değer ön görmez. Fizikteki tabiriyle Higgs kütlesi Standart Model açısından bir *serbest parametre*dir. Eğer durum böyleyse, neden bu serbest parametreyi deneysel olarak ölçülen değere eşitleyip mutlu mesut bir şekilde hayatımıza devam etmiyoruz?

Öncelikle, serbest parametreler kuramsal fizikçiler için rahatsız edicidir. İyi bir kuramın mümkün olduğunca az serbest parametreye sahip olmasını bekleriz. Ama asıl sorun bu değil. Durum şu ki, *yalın kütle* dediğimiz ve dışarıdan tanımlanan serbest parametre ile deneyde ölçtüğümüz fiziksel kütle birbirinden farklı nesneler. Standart Model yardımıyla Higgs'in gerçek kütlesini hesaplamak için yalın kütle terimine *kuantum düzeltmeleri* adı verdiğimiz birtakım terimler eklememiz gerekir. Eğer yalın kütleyi \\( \tilde{m}_h \\), fiziksel kütleyi \\( m_h \\) ile gösterirsek, aşağıdaki ifadeyi yazabiliriz:

$$ m_h^2 \sim \tilde{m}_h^2 + \Lambda^2$$

Bu denklemde \\( \Lambda^2 \\) kuantum düzeltmelerini temsil ediyor. Yalın kütleyi serbest olarak seçebilmemize karşın, eklemek zorunda olduğumuz bu terimlerin ne olması gerektiğini Standart Model bize söyler. Hiyerarşi Problemi de tam olarak burada ortaya çıkıyor. Standart Model'e göre \\( \Lambda \\)'nın değerinin 10<sup>19</sup> GeV civarında olması gerekir, yani Higgs kütlesinden 10<sup>17</sup> kat daha büyük. Elbette ki serbest parametre olan \\( \tilde{m}_h \\)'yi ayarlayarak fiziksel Higgs kütlesini deneydeki kütlesine eşitleyebiliriz, ancak bunu 10<sup>-34</sup> civarında bir hassasiyetle yapmamız gerekir.

Eğer bir kuramdaki herhangi bir serbest parametrenin bu denli hassasiyetle ayarlanması gerekiyorsa, o kuramın bir *ince ayar (fine tuning)* problemi var tabiri kullanılır. Standart Model'deki Hiyerarşi Problemi de bu sorundan kaynaklanıyor. Higgs kütlesi çok hassas bir şekilde ayarlanmış durumda, ama bunu açıklayan bir mekanizma Standart Model içerisinde yok.

## Güçlü CP Problemi ##

Standart Model'deki diğer bir ince ayar problemi de *Güçlü CP Problemi*dir. Standart Model'in ilk adımları atılmaya başlandığında, parçacıkların birbirleriyle olan etkileşimlerinin *CP simetrisi*ne sahip oldukları düşünülüyordu. Bunun anlamı, bir çarpışma deneyinde tüm parçacıkları anti-parçacıklar, anti-parçacıkları da parçacıklar ile değiştirip, bunların hareket yönlerini de tersine çevirdiğimizde, yaptığımız deneyin sonucunun aynı olması gerekiyordu. Ancak yapılan deneylerde zayıf etkileşimlerin bu simetriye uymadığı keşfedildi. Bu da demek oluyordu ki, *CP* Standart Model'in bir simetrisi değildi.

Eğer *CP* simetrisi zayıf etkileşimlerde korunmuyorsa, güçlü etkileşimlerde korunması için bir neden yok. Dolayısıyla, güçlü etkileşimlerde bu simetrinin kırılma derecesini temsil edecek bir serbest parametre tanımlayabiliriz. Bu parametreyi \\( \theta \\) harfi ile gösterelim. Higgs kütlesinin aksine, bu parametre için kuantum düzeltmeleri uygulamamıza gerek yok, direk deney ile bağdaştırabiliriz. Eğer
\\( \theta \\) sıfırdan farklıysa, nötronun bir elektrik dipol momentinin olması gerekir. Ancak bugüne kadar yapılan gözlemlerde böyle bir durumla karşılaşılmadı. Yani \\( \theta \\) terimi ya tam olarak sıfır, ya da sıfıra çok yakın bir değere sahip. Bu terimin neden bu kadar küçük (veya sıfır) olduğu, başka bir deyişle *CP* simetrisinin güçlü etkileşimlerde neden ihlal edilmediği sorusu *Güçlü CP Problemi* olarak tanımlanır.

Madem deneyde gözlemlemiyoruz, neden bu terimi güçlü etkileşimlerin kuramına ekliyoruz diye bir soru yöneltilebilir. Bunun birkaç cümle ile açıklayabileceğim basit bir nedeni yok. Şimdilik, Kuantum Renk Dinamiği'nin matematiksel yapısının bu terimi gerekli kıldığını belirteyim. İleriki yazılarımda bu konuya detaylı değinmeyi planlıyorum.

## Karanlık Madde ##

1970'li yıllarda çeşitli spiral galaksilerde yıldızların galaksi merkezi etrafındaki dönüş haraketlerini inceleyen bilim insanları ilginç bir sonuca ulaştılar. Newton mekaniğine göre, yıldızların merkeze olan uzaklığı arttıkça yörünge hızlarının da düşmesi gerekir. İçerisinde bulunduğumuz Güneş Sistemi'nde de durum böyledir. Ancak yapılan gözlemler, yörünge hızının uzaklığa göre düşmediğini, hatta az da olsa arttığını ortaya koydu. Bir şekilde galaksilerde, şu ana kadar gözlemleyemediğimiz ve bildiğimiz herhangi bir parçacık ile açıklayamadığımız bir maddenin olması gerekiyordu.

Fizikçiler bu maddeye *karanlık madde* adı verdiler. *Karanlık* teriminin nedeni ise, bu maddenin ışık ile hiç etkileşime girmiyor oluşu (veya gözlemleyemeyeceğimiz kadar zayıf). Aksi taktirde ışığa dayalı astronomi gözlemlerinde varlığını tespit edebilmemiz lazımdı. Galaksi kümelerinin davranışları üzerine yapılan gözlemler ve Kozmik Arkaplan Işıması'nın analizlerinden elde edilen sonuçlar, karanlık madde hipotezinin iyice güçlenmesine sebep oldu.

Yapılan son incelemeler, *baryonik madde* dediğimiz, Standart Model tarafından açıklanabilen maddenin evrenin yalnızca %5'ini oluşturduğunu bize gösteriyor. Karanlık maddenin oranı ise %27 civarında. Geri kalan kısım ise, daha büyük bir gizem olan ve benim yazıda hiç değinmediğim *karanlık enerji*.

Tahmin edilebileceği gibi, Standart Model'de karanlık madde rolünü üstlenebilecek herhangi bir parçacık yok. Dolayısıyla karanlık madde, Standart Model ötesinde bir fiziği aramamız için en önemli motivasyonlardan birisi olarak karşımıza çıkıyor.

## Kütleli Nötrinolar ##

Standart Model içerisinde *nötrino* adı verdiğimiz ve \\( \nu_e \\), \\( \nu_{\mu} \\), \\( \nu_{\tau} \\) ile gösterilen üç adet parçacık bulunuyor. Bu parçacığın temel özellikleri yüksüz oluşu ve madde ile çok ama çok sayıf bir şekilde etkileşmesi. Standart Model'e göre bu parçacıkların kütlesiz olması gerekir. Bunun sonucunda Standart Model içerisinde bu üç parçacığın birbirlerine *dönüşmesine* neden olarak herhangi bir etkileşimin bulunmaması gerekir.

Ancak 1998 yılında, Japonya'da yapılan *Super-Kamiokande* deneyinde, nötrinoların birbirlerine dönüşebildiği fark edildi. Büyük heyecan ve şaşkınlık uyandıran bu sonuç, daha sonra farklı deneyler ile teyid edildi. Eğer böyle bir dönüşüm mümkünse, nötrinoların kütleli olması gerekir. Kütleli nötrinolar, Standart Model ötesindeki fizik araştırmalarında temel başlangıç noktalarından birisidir.

## Neler Oluyor? ##

Günümüz kuramsal fiziğinin en önemli isimlerinden biri olan Nima Arkani-Hamed'in, geçtiğimiz yaz ayında Princeton'daki *Institute of Advanced Study*'de düzenlenen *Prospects in Theoretical Physics* adlı yaz okulunda verdiği bir dersin gayri-resmi adı *What the Hell is Going On? (Neler oluyor?)* idi. Nima, yukarıda değindiğim sorunlara önerilen çözümleri özetleyip kendi geliştirdiği modelleri anlattığı konuşmasında, kendisinin başka [toplantılarda](https://www.youtube.com/watch?v=ypKB7-o9s00) da değindiği bir noktayı tekrarladı. Son 40 yılda Kuramsal Yüksek Enerji Fiziği'nde öne sürdüğümüz ve doğru olduğuna neredeyse emin olduğumuz modeller (örneğin *süpersimetri*), deneyler tarafından doğrulanamadı. Bu durumun, Kuramsal Yüksek Enerji Fiziği'ni bir krize soktuğunan söz edebiliriz. Ancak bu genç kuramsal fizikçi adayları için iyi bir fırsat da olabilir. Çünkü çözülmesi gereken bir çok problem var, ve kuramsal fizik dünyasının taze fikirlere ihtiyacı var.

Bu yazımda, Standart Model'in sorunlarına kısaca bir giriş yaptım. İleriki yazılarımda her bir konuyu ayrıntılı bir şekilde ele alıp çözüm önerilerinden bahsedeceğim. Bu yazı dizisinin, Kuramsal Yüksek Enerji Fiziği'nin gizemli ve heyecan verici dünyasına adım atmak isteyen bilim insanı adayları için faydalı olacağını umut ediyorum.
