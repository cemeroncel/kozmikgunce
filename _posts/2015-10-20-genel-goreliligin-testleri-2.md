---
layout: post
title: Genel Göreliliğin Testleri 2
tags: [görelilik]
author: Devin Çeşmecioğlu
archive: true
---
100 yıllık çınar hakkında konuşmaya devam edelim. Önceki yazıya [buradan]({{ site.baseurl }}{% post_url 2015-09-29-genel-goreliligin-testleri-1 %}) ulaşabilirsiniz. Hatta ulaşmalısınız çünkü bir noktaya kadarki işlemleri tekrar etmek yerine, önceki yazıdan neyin farklı olduğunu söyleyeceğim.

## Işığın Bükülmesi

Bir teoriye ününü asıl kazandıracak olay, henüz gözlenmemiş bir olguyu öngörmesi ve doğru çıkarımlarda bulunmasıdır.

Albert Einstein, Güneş’in yarattığı uzay-zaman eğriliğinin, fotonun Güneş etrafındaki yörüngesini nasıl etkileyeceğini merak etti.

![Işığın Bükülmesi]({{ site.url }}/assets/deflection.gif)

Işığın, fotonun, kütleçekimi etkisinde izleyeceği yolu bulmak için uzay-zaman kavramına bakarız. \\( ds^2 \\) ifadesi kütlesiz nesneler için \\( 0 \\)‘dır ve bir önceki bölümde yapılan işlemler buna göre tekrar edilebilir.

$$ ds^2 = 0 , \quad u'' + u = 3 G M u^2 $$

Güneş ile fotonun bir çeşit saçılma yaşayacağını bildiğimizden ve bu saçılmanın da epey küçük olmasını beklediğimizden, çarpışma parametresini yaklaşık olarak tanımlarız.

![Fotonun güneşten saçılması]({{ site.url }}/assets/defl_alpha_sun.gif)

$$ d \simeq r \sin \alpha, \quad u \simeq \simeq \frac{\sin \alpha}{d} $$

Önceki yazıda geçen \\( u=1/r \\) dönüşümünü ve \\(d \\)  parametresini kullanarak iki önceki denklemi yeniden yazabiliriz.

$$ u'' + u = 3 G M u^2 \simeq 3 M G \left(\frac{\sin^2 \alpha}{d^2}\right)  = \frac{3 M G}{d^2}(1-\cos^2 \alpha) $$

Bu denklemin bir çözümünü dilersek [şuradan](https://www.wolframalpha.com) çıkartabiliriz (söz konusu site ve kullanımı hakkında da bir yazı yazmayı düşünüyorum).

$$ u = \frac{\sin \alpha}{d} + \frac{3GM}{d^2} \left(1 + \frac{1}{3}\cos 2 \alpha\right) $$

Foton Güneş’ten yeterince uzaklaştığında \\(\alpha\\)  açısının \\(0\\)‘a gitmesini beklemeyiz. Oldukça ufak da olsa sonlu bir değere yakınsayacaktır. Küçük açı yaklaşımları ile denklem iyice sadeleşir.

$$ \sin \alpha \simeq \alpha , \quad \cos 2\alpha \simeq 1, \quad u \simeq \frac{\alpha}{d} + \frac{2GM}{d^2}=0 $$

Fotonun yörüngesini, Güneş’e en yakın olduğu konumdan itibaren inceledik ancak olayın simetrisinden dolayı, bizim asıl aradığımız bu sapma açısının iki katıdır.

$$ \boxed{\delta = 2 \vert \alpha \vert = \frac{4GM}{d c^2}} $$

## Son Sözü Gözlem Söyler

Hesap kolaylığı için, sabit olan ve üşengeçlikten yazmadığımız ışık hızını da ait olduğu yere koyduk. Schwarzschild yarıçapı olarak tanımlanan \\(R_S\\)  ve Güneş’in yarıçapını temsil eden \\(R_{\odot}\\) ile daha da kolay okunacak hale getirebiliriz.

Sayısal değerleri yerleştirirken dikkat edilmesi gereken iki unsur, küçük açı yaklaşımı yapıp \\(\sin \alpha \simeq \alpha\\)  olarak çıkardığımız ifadenin radyan biriminde olduğunu hatırlayıp, radyan-derece dönüşümü yapmak ve bu olaya ilişkin gözlem yapılabilmesi için \\(R_{\odot}\\)  ile \\(d\\)‘nin birbirine çok yakın olmasını şart koşmak.

$$ \delta = 2 \times \frac{3\;\textrm{km}}{700,000\; \textrm{km}} \left(\frac{180}{\pi}\right) \frac{R_{\odot}}{d} \simeq 1.75'' $$

Einstein tarafından ulaşılan, açısaniyesi birimindeki bu değer Newton kütleçekimiyle ulaşılandan farklı bir öngörüydü. 29 Mayıs 1919’da, tam Güneş tutulması sırasında Arthur Eddington tarafından sınandı ve onaylandı. Genel Görelilik fikrinin tescillenmesi ve Einstein’in dünyaca üne kavuşması, işte bu gözlemin ardından geldi.
