---
layout: post
title: Winter is here!
author: Devin Çeşmecioğlu
comments: true
---

Bin yılı aşkın sessizliğin ardından hayata döndük. KozmikGünce, Wordpress ortamından çıkıp kendi alan adıyla yayına devam etmeye hazır.
Eski, yeni, rafta unutulmuş veya gündemdeki önemini koruyan, yaşama ve evrene dair fiziksel konularda buluşmak üzere.

![Bender Bending Rodriguez]({{ site.url }}/assets/we-are-back.jpg)
