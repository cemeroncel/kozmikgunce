---
layout: post
title: Feynman Serisi
tags: [tarih]
author: Devin Çeşmecioğlu
archive: true
---
Yıllar evvel, Reid Gower isimli biri tarafından hayata geçirilmiş güzel bir proje vardı: Carl Sagan'ın Cosmos belgesel serisinden seçme pasajlar alıp, etkileyici görüntüler ve müziklerle yeniden kurgulamak.

Reid'i bir Youtube ünlüsü haline getiren projenin sayfasına ve tabi ki vidyolara [buradan](http://saganseries.com/) bakabilirsiniz.

Başlıkta adı geçen ise aynı kişi tarafından yapılan ikinci bir proje: Feynman Serisi. İlk vidyosu güzellik üzerine olan çalışmanın dört bölümü daha var.

<iframe width="560" height="315" src="https://www.youtube.com/embed/cRmbwczTC6E?list=PL92F9FC91BBE2210D" frameborder="0" allowfullscreen></iframe>

20\. yüz yılın en etkin fizikçilerinden Richard P. Feynman, [BBC Horizon](http://www.bbc.co.uk/programmes/b006mgxf/episodes/guide) belgesellerine birkaç kez konuk oldu. Feynman Serisi için de bu konuşmalar baz alınmış. Estetik, felsefe, insan doğası ve doğanın insanı (?) üzerine havalı olmayan laflarla yapılmış derin gözlemler dinlemek isteyenler için birebir. Ünlü fizikçinin yer aldığı veya ona dair çekilmiş belgesel/film/vidyoların derlendiği bir de [şöyle](http://www.richard-feynman.net/videos.htm) bir site var.

Richard Feynman'a dair her nedense pek az bilinen, [Infinity](http://www.imdb.com/title/tt0116635/?ref_=fn_al_tt_1) adında bir de film çekilmiş. 2. Dünya Savaşı yıllarında yaşadıkları anlatılırken fizikçiyi canlandıran, aynı zamanda filmin yönetmeni de olan Matthew Broderick ve Feynman'ın ilk eşini oynayan da, bu sene en iyi yardımcı kadın oyuncu dalında Oscar kazanan Particia Arquette. Filmin senaryosu büyük ölçüde bir kitaptan uyarlanmış. Son kez Alfa Yayınları'ndan çıkan kitabın ismi [Eminim Şaka Yapıyorsunuz Bay Feynman](http://www.amazon.com/Eminim-Saka-Yapiyorsunuz-Bay-Feynman/dp/6051066756/ref=sr_1_3?s=books&ie=UTF8&qid=1435871146&sr=1-3&keywords=bay+feynman). Kitapta, nerdeyse tüm kitaplarında olduğu gibi, yazarın hayatından ilginç kesitler yer alıyor.

Bay Feynman'ın yeni nesil fizikçilere en önemli mirası Feynman Fizik Dersleri hakkında (en azından kısmen) birşeyler okumak isteyen olursa, güzel bir başlangıç Arif Bayırlı'nın [Gök Günce](http://www.gokgunce.net/2014/05/yeni-mezundan-fizik-lisans-tavsiyeleri.html) sitesi.

Feynman'ı fizikçiler arasında neyin ünlü yaptığından ise önümüzdeki haftalarda bahsederiz.

![Feynman]({{ site.url }}/assets/feynman.jpg)
