---
layout: post
title: Floransa Günlükleri - Hafta 2
subtitle: GGI 2018 Kış Okulu'ndan İzlenimler
tags: [standart-model parçacık-fiziği]
author: Cem Eröncel
comments: true
published: true
share-img: /assets/firenze.jpg
---
GGI 2018 kış okulu izlenimlerine okulun ikinci haftası ile devam ediyoruz. Bu haftanın dersleri Csaba Csaki'den *Standart Model ötesi üzerine konular* ve Lance Dixon'dan *Amplitudeology*.

## Standart Model üzerine konular ##

*Neler Oluyor?* başlıklı [yazımda]({{ site.baseurl }}{% link _posts/2017-12-10-neler-oluyor.md %}), Standart Model'in en büyük sorunlarından biri olarak, *Hiyerarşi problemini* örnek göstermiştim. Kısaca özetlemek gerekirse, Standart Model bize Higgs'in kütlesinin, kuramın geçerli olduğu en üst enerji seviyesi (*cutoff scale*) düzeyinde olmasını söyler. Ancak Higgs'in ölçülen kütlesi bu değerin çok altındadır. 

Csaba Csaki'nin dersi, bu soruna olası çözüm önerilerinin sıralanması ile başladı. İlk yazımda bunlara değinmediğim için burada kısa bir özet geçmekte fayda var.

- **Süpersimetri:** Bu kuramın özelliği, her parçacığın *süper eş* dediğimiz bir adet de *süper parçacığı* olmasıdır. Bir bozonun süper eşi bir fermiyon, bir fermiyonun süper eşi de bir bozondur. Parçacıklar arasındaki bu yeni *süpersimetri*, kuramın yapısı gereği Higgs'in kütlesinin düşük olmasını sağlar. 

- **Karma Higgs Modelleri:** Bu tarz kuramlar Higgs parçacığını temel bir parçacık olarak ele almak yerine, çeşitli güçlü etkileşim dinamikleri sonucu oluşan bir *bağlı durum (bound state)* olarak ifade ederler. Elektrozayıf simetri kırınımı da bu olayın bir sonucu olarak ortaya çıkar. Higgs'i temel bir parçacık yerine *karma (composite)* bir parçacık olarak düşünerek, Hiyerarşi problemi'ni ortadan kaldırabiliriz. 

- **Ek boyutlu modeller:** Higgs kütlesi ile *cutoff scale* arasındaki büyük hiyerarşi, kurama ek bir uzay boyutu eklemlenmesi ile doğal hale getirebilir. Eğer bu boyut sonlu ve özel bir geometriye sahipse (*AdS geometrisi*), büyük hiyerarşiler bu geometrinin yapısı sayesinde rahatlıkla açıklanabilir. 

- **Relaxion:** Higgs kütlesi temel kuramda büyük olmasına karşın, kozmik devinimler sonucu bu zamanki küçük değerine ulaşmıştır. 

- **Antropik İlke:** İçinde yaşadığımız evren, var olan milyonlarca evrenden yalnızca bir tanesidir. Higgs'in kütlesi her evrende farklı bir değer alır. Yaşadığımız evrendeki Higgs kütlesinin küçük olması, bu kuram içinde istatistiksel olarak açıklanabilir. 

Csaba Csaki, derslerinde *Karma Higgs Modelleri* ve *Ek boyutlu modeller* üzerinde durdu. İlk olarak, bu modellerin temelini oluşturan *Goldstone Teoremi* işlendi, ardında da bu modellerin bir analojisi olarak, Kuantum Renk Dinamiği'ndeki *kiral simetrinin (chiral symmetry)* kırınımı sonucu kuarkların nasıl bağlı durumlar oluşturduğu analiz edildi. Daha sonra, aynı mantığın nasıl Higgs parçacığına uygulanabileceği üzerine duruldu. Son olarak da ek boyutlu modellerin açıklanması ile ders sona erdi. 

Derste özellikle bir kaynak listesi belirtilmedi. Kendim derlediğim bir kaynak seçkisi şöyle:

- R. Rattazzi, [arXiv:hep-ph/0607055](https://arxiv.org/abs/hep-ph/0607055) (2006).
- T. Gherghetta, [arXiv:hep-ph/1008.2570](https://arxiv.org/abs/1008.2570) (2010).
- C. Csaki, J. Hubisz, and P. Meade, [arXiv:hep-ph/0510275](https://arxiv.org/abs/hep-ph/0510275) (2005).
- C. Csaki, [arXiv:hep-ph/0404096](https://arxiv.org/abs/hep-ph/0404096) (2004).
- C. Csáki, C. Grojean, and J. Terning, [Rev. Mod. Phys. 88, 045001](https://journals.aps.org/rmp/abstract/10.1103/RevModPhys.88.045001) (2016).
- C. Csáki and P. Tanedo, [arXiv:hep-ph/1602.04228](https://arxiv.org/abs/1602.04228) (2016).

## Amplitudeology ##

Türkçe bir karşılık bulamadığım *Amplitudeology*, kısaca Kuantum Alan Kuramı'ndaki *saçılım olasılıklarının (scattering amplitudes)* sistematik olarak çalışılmasıdır. LHC ve Fermilab gibi hızlandırıcılarda yapılan deneyler, iki veya daha fazla parçacığın birbirleri ile yüksek hızlarda çarpıştırılması üzerine kuruludur. Deneylerden elde edilen veriler ışığında, çarpışma sonucunda hangi parçacıkların hangi olasılıklarla ortaya çıktığı bilgisine ulaşılır. Bu olasılıklara *saçılım olasılıkları* adı verilir. 

Herhangi bir parçacık fiziği kuramını hızlandırıcılarda test etmenin yolu, kuramın öngördüğü saçılım olasılıklarını deneysel olarak elde edilen değerlerle karşılaştırmaktır. Dolayısıyla, bu olasıklıkları hassas bir şekilde hesaplayabilmek büyük önem taşır. 

Bu hesapları yapmanın en popüler ve bilinen yolu *Feynman diagramları* kullanmaktır. Bu yöntem, bir çok saçılım olasılığını hesaplamak için ideal olsa da, etkileşimler daha zorlaştıkça verimliliğini kaybeder. Bu yüzden son yıllarda, bu yönteme alternatif yollar geliştirmek, kuramsal fiziğin önemli bir araştırma alanı haline geldi. *Amplitudeology*, bu yöntemlerin genel bir adı olarak tanımlanabilir. 

Bu tarz bir çalışmanın ana motivasyonu, Kuantum Renk Dinamiği'ndeki (KRD) saçılım olasılıklarını daha hassas bir şekilde hesaplamaktır. Düşük enerjilerde, KRD etkileşimleri güçlü hale geldiğinden, Feynman diagramları ile hassas hesaplar yapmak gittikçe işin içinden çıkılmaz bir hale gelir. Ancak bu tür hesaplamalar, Standart Model'in limitlerini sınamak açısından büyük önem taşır. Standart Model'in öngörülerinin LHC sonuçları ile karşılaştırılması için, şu anda elimizde bulunanlardan daha hassas hesaplara ihtiyacımız var ve bu tarz hesaplar ancak yeni yöntemlerle mümkün olabiliyor. 

Lance Dixon'un dersi, bu tarz hesaplarda kullanılan çeşitli tekniklerin bir özetinden ibaretti. Tekniklerin matematiksel altyapısının incelenmesi yanında, çeşitli kuramlara nasıl uygulananabileceğinden de bahsedildi. 

Dürüst olmak gerekirse, kendi açımdan pek aşina olmadığım ve bana oldukça teknik gelen bir ders oldu. Dersten pek birşey anladığımı söyleyemem, ama bu tarz çalışmaların yapılıyor olduğunu bilmek de yeter. 

İlgilenenler için derste verilen birkaç kaynak:

- Z. Bern, J.J.M. Carrasco, and H. Johansson, Physical Review D 78, (2008) [arXiv:hep-ph/0805.3993](https://arxiv.org/abs/0805.3993).
- C. Duhr, [arXiv:hep-ph/1411.7538](https://arxiv.org/abs/1411.7538) (2014).
- L.J. Dixon, [arXiv:hep-ph/1310.5353](https://arxiv.org/abs/1310.5353) (2013).
- L. Dixon, [arXiv:hep-ph/9601359](https://arxiv.org/abs/hep-ph/9601359) (1996).
- H. Elvang and Y. Huang, [arXiv:hep-ph/1308.1697](https://arxiv.org/abs/1308.1697) (2013).

## Gelecek Yazı ##

Gelecek yazımda, okulun son haftasında verilen dersleri özetleyerek bu yazı dizisini tamamlayacağım. 




