---
layout: post
title: Einstein ve Eddington
tags: [tarih]
author: Devin Çeşmecioğlu
archive: true
---
Albert Einstein ve 20. yüz yılın başlarında İngiltere’deki en iyi ölçümcülerden diye bilinen Arthur Eddington. Bambaşka karakterlerde iki insan.

BBC’nin 2007’de televizyon filmi olarak çektirdiği [Einstein ve Eddington](http://www.imdb.com/title/tt0995036/), Genel Görelilik kuramının ilk günlerini ve dünyaya duyrulmasını, 1. Dünya Savaşı arka planında anlatıyor. İzlemenizi tavsiye edemiyorum çünkü filmi bulmak epey zor.

![Einstein ve Eddington]({{ site.url }}/assets/einstein-ve-eddington.jpg)

Hikayenin fizik açısından can alıcı iki noktası var. Bu yazıda bahsedeceğimiz ilkinde, Arthur henüz yalnızca Özel Görelilik’ten haberdar.

## Özel Görelilik

Albert E. elektrodinamikte karşılaşılan bir tutarsızlığa çözüm bulmaya çalışırken, bugün bir devrim olarak gördüğümüz ilk adımları attı. İki durumu düşünelim:

- İletken ve düz bir tel, uzandığı doğrultu boyunca bize göre sabit hızla hareket etsin. Maxwell bağıntıları uyarınca hem bir elektrik akımı göreceğiz hem de teli çevreleyen bir manyetik alan.
- İletken ve düz bir tel dursun ve biz telin uzandığı doğrultuda tele göre sabit hızla hareket edelim. Telin görece hareketi nedeniyle yine bir elektrik akımı görmemiz gerekir. Peki manyetik alan da görür müyüz?

İki tablonun doğa açısından eşdeğer olması gerektiğini düşünen Albert, bunu sağlamanın bir yolunu buldu. İki farklı olayı gözleyen iki gözlemciyi ele alalım. Önceleri, olaylar arasında ne kadar zaman geçtiği konusunda hemfikirlerdi. Bunun yanı sıra olaylar arasında ne kadar mesafe olduğu konusunda da anlaşıyorlardı.

$$ \Delta t_1 = \Delta t_2 $$

$$ \Delta x_1 = \Delta x_2 $$

Ancak gözlemlerle oldukça uyumlu Maxwell denklemlerini kurtarmak için iki aksiyomu benimsemek gerekmişti.

- Doğa yasaları her eylemsiz gözlemci için aynı sonuçları öngörür
- Tüm eylemsiz gözlemciler ışığın boşluktaki süratini aynı ölçer

Albert bu aksiyomları da heseba kattığında, iki (eylemsiz) gözlemcinin üzerinde anlaşması gereken ölçümün, zamanın ve konumun ilginç bir kombinasyonu olması gerektiğini farketti.

$$ {(\Delta s)^2} = {(\Delta x_1)^2} - {(\Delta ct_1)^2} = {(\Delta x_2)^2} - {(\Delta ct_2)^2} $$

Eğer ışığın sürati eylemsiz tüm gözlemciler için aynı olacaksa, uzay-zaman aralığı (space-time interval) denen bu nesne, boşluktaki ışık için, sabit olmalıydı. Madem sabit olacaktı, \\(\Delta x / \Delta t = \pm c \\) ifadesini sağlayacak şekilde olsundu.

$$ {(\Delta s_{foton})^2} = 0 $$

Aksiyomlar ve matematik yerli yerine oturunca, hiçbir etkileşmenin ışıktan hızlı gerçekleşemeyeceği sonucuna ulaşmak kolaydı - demek isterdim. O güne kadar geçerli olan Newton kütleçekim kurallarına göre, dedi Arthur, Güneş bir anda yok oluverse Dünya'nın o anda yörüngesinden kurtulması gerekir ancak anında etkileşme diye bir şey yoksa, denklemlerin de bunu yansıtması gerekir.

![Eddington]({{ site.url }}/assets/einsteineddington3.jpg)

Denklemlere bunun nasıl yansıdığını görmek ileriki bir yazıya kalsın. Şimdi ufak bir hesapla ışığın Dünya-Güneş mesafesini ne kadar sürede katettiğine bakalım.

## Genel Görelilik

Bu hesap için hemen aklımıza gelen formülü kullanabiliriz.

$$ x = vt $$

Burada \\(x\\) Dünya-Güneş mesafesi, \\(v\\) ışığın sürati ve \\(t\\) de aradığımız süre değeri olursa, yaklaşık \\(8.294444 \, dk \\) buluruz. Genel Görelilik'te durum nasıl?

Karl Schwarzschild, 1916 yılında yeni kütleçekim kuramının ilk küresel simetrik çözümünü buldu. Gök cisimlerine uygulanabilecek çözüm, az önce bahsettiğimiz uzay-zaman aralığının ortamda küresel simetrik bir nesne varken ne hale geleceğini gösterdi. Artık \\(\Delta s\\) yerine Calculus'teki sonsuz küçükler ile ifade ediliyordu.

$$ ds^2 = - \left( 1 - \dfrac{2GM}{c^2 r} \right) c^2 dt^2 + \left( 1 - \dfrac{2GM}{c^2 r} \right)^{-1} dr^2 $$

Açısal kısımları göz ardı ettik ve sadece doğrudan iki nesne arasında radyal yol alan fotonlara bakıyoruz.

Fotonların sağladığı koşulu uygulayınca, elimizde şöyle bir integral kalıyor.

$$ {\displaystyle \int_{0}^{t} c dt^{\prime}} = {\displaystyle \int_{R}^{AU}\left( 1 - \dfrac{2GM}{c^2 r} \right)^{-1} dr } $$

Burada da \\(R\\) Güneş'in yarıçapı (çünkü yıldızın yüzeyinden kopup gelen fotonlarla ilgileniyoruz), \\(AU\\) 1 astronomik birim yani ortalama Dünya-Güneş mesafesi, \\(G\\) Newton'un kütleçekim sabiti ve \\(M\\) de Güneşin kütlesi.

\\(2GM<<R\\) olduğundan, \\(x < < 1\\) durumunda kullanılabilecek aşağıdaki ifadeden faydalanabiliriz.

$$ (1 + x)^n \simeq 1 + nx $$

Nihayetinde sayısal değerleri de monte edebiliriz.

$$ t \simeq \dfrac{1}{c} \left( r + \dfrac{2GM}{c^2} \ln(r) \right)\Big|_{R}^{AU} \simeq 8.294445 \, dk $$

Yeni kuram henüz gerçek gücünü göstermeye başlamadı. Bu nedenle Newton yaklaşımıyla (neredeyse) aynı sonucu bulmamız normal. Şimdilik ne tür matematiksel nesnelerle uğraşacağımızı görmeye başladık.
