---
layout: post
title: Floransa Günlükleri - Giriş
subtitle: GGI 2018 Kış Okulu'ndan İzlenimler
tags: [standart-model parçacık-fiziği]
author: Cem Eröncel
comments: true
published: true
---
8 - 27 Ocak 2018 tarihleri arasında Galileo Galilei Enstitüsü'nde gerçekleştirilecek olan *GGI Lectures on the Theory of Fundamental Interactions (Temel etkileşimlerin kuramı üzerine GGI Dersleri)* adlı kış okuluna katılmak için İtalya'nın Floransa kentindeyim. Haftaiçi her gün, günde iki ders artı bir adet alıştırma seansından toplam üç hafta sürecek olan okulun amacı, günümüz kuramsal yüksek enerji fiziğinde yer alan çeşitli konuların temel kavramlarını derin ve ileri düzeyde katılımcılara aktarmak. 

Okulun toplam süresinde, her hafta ikişer konu olmak üzere toplam 6 farklı konu farklı konuşmacılar tarafından işlenecek. Konular ve haftalara dağılımları şu şekilde:

**1. Hafta**

- Elektrozayıf etkileşimlerin standart modeline bir giriş - *Giovanni Ridolfi (Genova U. & INFN)*
- Efektif Alan Kuramı'nın yeni uygulamaları - *Ira Rothstein (Carnegie Mellon U.)*

**2. Hafta**

- Standart Model ötesi üzerine konular - *Csaba Csaki (Cornell U.)*
- Amplitudeology - *Lance Dixon (SLAC)*

**3. Hafta**

- Enflasyon ve Kozmolojik Pertürbasyonlar - *Antonia Riotto (Geneva U.)*
- Karanlık madde *Tracy Slatyer (MIT)*

Önümüzdeki üç hafta boyunca, okulda edindiğim izlenimler ve öğrendiğim konular hakkında yazılar yazmayı planlıyorum. Hedefim, her hafta bir yazı koymak. Eğer yukarda yazdığım konulara ilgi duyduysanız önümüzdeki haftalarda gözünüz bu güncede olsun. 

*Not:* Çeşitli ders notlarının da olduğu okulun internet sayfasına [buradan](http://webtheory.sns.it/ggilectures2018/) ulaşabilirsiniz. 
