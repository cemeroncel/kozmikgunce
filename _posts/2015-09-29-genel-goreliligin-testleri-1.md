---
layout: post
title: Genel Göreliliğin Testleri 1
tags: [görelilik]
author: Devin Çeşmecioğlu
excerpt: "Kepler'den beri gezegenlerin eliptik yörüngelere sahip olduğu biliniyordu ancak Merkür'ün yörüngesinde bir sorun olduğu da biliniyordu. Yörüngenin Güneş'e en yakın noktası, perihelion, her yıl biraz kayıyordu."
archive: true
---
## Merkür'ün Yörüngesi

Kepler'den beri gezegenlerin eliptik yörüngelere sahip olduğu biliniyordu ancak Merkür'ün yörüngesinde bir sorun olduğu da biliniyordu. Yörüngenin Güneş'e en yakın noktası, perihelion, her yıl biraz kayıyordu.

Sorunun kaynağı elbette Güneş Sistemi'ndeki diğer nesnelerdi. Tüm bilgiler ışığında hesapları titizlikle yapan Le Verriere, 1851 yılında, kaymanın bir kısmının yine de açıklanamadığını ilan etti. Bu bilmeceyi çözen Einstein olacaktı.

## Newton kütleçekimine göre yörüngeler

Küresel kordinatlarda, Güneş yörüngesindeki Merkür'ün konumunu \\(r\\) ve $latex \\(\phi\\) ile belirleyelim ve \\(\theta=\pi/2\\) düzlemine bakalım. Bu halde Lagrange fonksiyonu,

$$ \mathcal{L} = \dfrac{1}{2} \dot{r}^2 + \dfrac{1}{2} r^2 \dot{\phi}^2 + \dfrac{GM}{r} $$

Newton hareket denklemlerine eşdeğer olan Euler-Lagrange denklemleri,

$$ \dfrac{d}{dt} \dfrac{\partial \mathcal{L}}{\partial \dot{\phi}} = \dfrac{\partial \mathcal{L}}{\partial \phi} $$

Değişkenin üzerindeki nokta, zaman türevinin kısaltması.

$$ \dot{\phi} \equiv \dfrac{d \phi}{dt} $$

Euler-Lagrange denklemleri sayesinde hareket sabitlerini buluruz.

$$ \dfrac{\partial \mathcal{L}}{\partial \phi} = 0, \qquad \dfrac{\partial \mathcal{L}}{\partial \dot{\phi}} = r^2 \dot{\phi} \equiv L $$

Radyal işlemler de bize çözmemiz gereken denklemi verir.

$$ \dfrac{d}{dt} \dfrac{\partial \mathcal{L}}{\partial \dot{r}} = \dfrac{d \dot{r}}{dt} = \ddot{r} = r \dot{\phi}^2 - \dfrac{GM}{r^2} $$

Bir değişken dönüşümü yaparsak denklem sadeleşir. Ayrıca istediğimiz, radyal kordinat ile açısal kordinatı birbirine bağlı yazmak.

$$ u=\dfrac{1}{r}, \qquad \dot{r} = r' \dot{\phi} = -u' L, \qquad \ddot{r} = -u'' u^2 L^2 $$

Burada \\(u'\\) notasyonu \\(u\\) fonksiyonunun \\(\phi\\)'ye göre türevidir. Dönüşümleri alıp (5) denklemine uygularsak,

$$ -2 u^2 u'' - 2 u^3 = - \dfrac{2GM}{L^2} u^2 $$

\\(u=1/r\\) olduğundan ve \\(u\\) fonksiyonunun \\(0\\) olamayacağını bildiğimizden, denklemi \\(-2u^2\\)'ye böleriz.

$$ \boxed{u'' + u = \dfrac{GM}{L^2}} \label{newton-orbit} $$

## Einstein geometrisine göre yörüngeler

Yeni uzay-zaman kavramındaki sonsuz küçük mesafe ifadesi,

$$ ds^2 = -(1-2GM/r)dt^2 + (1-2GM/r)^{-1}dr^2 + r^2 d{\theta}^2 + r^2 \sin^2 (\theta) d{\phi}^2 $$

Matrismiş gibi ifade edilebilen metrik katsayıları,

$$
g_{\mu \nu} =
\begin{bmatrix}
-(1-2GM/r) & 0 & 0 & 0 \\
0 & (1-2GM/r)^{-1} & 0 & 0 \\
0 & 0 & r^2 & 0 \\
0 & 0 & 0 & r^2 \sin(\theta)^2
\end{bmatrix}
$$

Hareket denklemlerine ulaşmak için kullanacağımız Lagrange yoğunluğu fonksiyonu,

$$ \mathcal{L} = \dfrac{1}{2} g_{\mu \nu} \dot{x}^{\mu} \dot{x}^{\nu} = - \dfrac{1}{2} $$

Burada, Genel Görelilik'te sıkça rastlanabilen \\(g_{\mu \nu} \dot{x}^{\mu} \dot{x}^{\nu} = -1\\) eşitliğini kullanabiliriz.

$$ 2 \mathcal{L} = -(1-2GM/r) \dot{t}^2 + (1-2GM/r)^{-1} \dot{r}^2 + r^2 \dot{\phi}^2 $$

Bir de Newton versiyonundaki gibi \\(\theta=\pi/2\\) düzleminde çalışmayı tercih edebiliriz ve değişkenlerin üzerindeki nokta notasyonu ile ifadeleri kısaltabiliriz.

$$ \theta = \dfrac{\pi}{2}, \qquad \dot{t} \equiv \dfrac{dt}{ds} $$

Euler-Lagrange denklemleri sayesinde yine hareket sabitleri belirleriz.

$$ - \dfrac{\partial \mathcal{L}}{\partial \dot{t}} = (1-2GM/r) \dot{t} \equiv E $$

$$ \dfrac{\partial \mathcal{L}}{\partial \dot{\phi}} = r^2 \dot{\phi} \equiv L $$

Hareket sabitleriyle denklemi biraz daha ufaltırız.

$$ \dot{r}^2 = E^2 - (1-2GM/r) \left( 1 + \dfrac{L^2}{r^2} \right) $$

(6) ile verilen dönüşümleri uygularsak denklem iyice sadeleşir.

$$ {u'}^2 + u^2 = \dfrac{E^2 - 1}{L^2} + \dfrac{2GM}{L^2} u + 2GM u^3 $$

Şimdi tüm denklemin \\(\phi\\)'ye göre türevini alalım.

$$ 2 u' u'' + 2 u u' = \dfrac{2GM}{L^2} u' + 6GM u^2 u' $$

\\(u'\\) fonksiyonu \\(0\\) olamayacağından tüm ifadeyi \\(2u'\\) ile bölebiliriz.

$$ \boxed{u'' + u = \dfrac{GM}{L^2} + 3GM u^2} $$

(7) ifadesiyle (18) arasındaki fark yalnızca \\(3GMu^2\\) terimidir. Einstein'in yeni yaklaşımıyla yörünge denklemine düzeltme olarak bulduğu bu terim sayesinde \\(100\\) yılda \\(43\\) açısaniyesi olan sapmanın, \\(42.98\\) açısaniyesi kadarına açıklık getirilmiş olur.
